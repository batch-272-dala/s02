CREATE DATABASE enrollment_db;

CREATE TABLE students (
	studentId INT NOT NULL AUTO_INCREMENT,
	studentName VARCHAR(50) NOT NULL,
	PRIMARY KEY (studentId)
);

CREATE TABLE teachers (
	teacherId INT NOT NULL AUTO_INCREMENT,
	teacherName VARCHAR(50) NOT NULL,
	PRIMARY KEY (teacherId)
);


CREATE TABLE courses (
	courseId INT NOT NULL AUTO_INCREMENT,
	courseName VARCHAR(50) NOT NULL,
	teacherId INT NOT NULL,
	PRIMARY KEY (courseId),
	CONSTRAINT fk_courses_teacherId
	FOREIGN KEY (teacherId)
	REFERENCES teachers(teacherId)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE student_courses (
	courseId INT NOT NULL,
	studentId INT NOT NULL,
	CONSTRAINT fk_student_courses_courseId
	FOREIGN KEY (courseId)
	REFERENCES courses(courseId)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	CONSTRAINT fk_student_courses_studentId
	FOREIGN KEY (studentId)
	REFERENCES students(studentId)
	ON UPDATE CASCADE
	ON DELETE RESTRICT	
);